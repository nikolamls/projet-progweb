const mongoose = require('mongoose')
//const path = require('path')
//const recetteImgChemin = 'uploads/recetteImage'

//schema = table où l'on va définir les colonnes
const recetteSchema = new mongoose.Schema({
    nomRecette: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    nbPersonne: {
        type: Number
    },
    nbCalorie: {
        type: Number
    },
    ingredients: {
        type: String,
        required: true
    },
    tpsPreparation: {
        type: String
    },
    prix: {
        type: Number
    },
    difficulte: {
        type: String
    },
    ustensiles: {
        type: String,
        required: true
    },
    imageRecette: {
        type: Buffer
    },
    imageRecetteType: {
        type: String
    },
    typeCuisine: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'TypeCuisine'
    },
    dateCreation: {
        type: Date,
        required: true,
        default: Date.now
    }
})

/*recetteSchema.virtual('recetteImgChemin').get(function() {
    if (this.imageRecette != null) {
        return path.join('/', recetteImgChemin, this.imageRecette)
    }
})*/

recetteSchema.virtual('recetteImgChemin').get(function() {
    if (this.imageRecette != null && this.imageRecetteType != null) {
        return `data:${this.imageRecetteType};charset=utf-8;base64,${this.imageRecette.toString('base64')}`
    }
})

//exporter le schema, TypeCuisine = nom de la table au sein de la bdd
module.exports = mongoose.model('Recette', recetteSchema)

//module.exports.recetteImgChemin = recetteImgChemin