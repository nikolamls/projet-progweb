const mongoose = require('mongoose')
const Recette = require('./recette')

//schema = table où l'on va définir les colonnes
const typeCuisineSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
})

//vérifier que lors de la suppression d une catégorie culinaire, celle-ci n est pas reliée à une ou plusieurs recettes
typeCuisineSchema.pre('remove', function(next) {
    Recette.find({typeCuisine: this.id}, (err, recettes) => {
        if (err) {
            next(err)
        } else if (recettes.length > 0) {
            next(new Error('Cette catégorie culinaire est reliée à une ou des recettes'))
        } else {
            next()
        }
    })
})

//exporter le schema, TypeCuisine = nom de la table au sein de la bdd
module.exports = mongoose.model('TypeCuisine', typeCuisineSchema)
