Bonjour Monsieur,

Ce GitLab repository est le projet avec le CSS mais sans passport.
Nous avons un deuxième Gitlab nommé Time2Cook où nous avons réalisé le projet avec l'authentification Passport, mais cependant sans CSS :
https://gitlab.com/sofiane.foudil/time2cook
Dessus, vous verrez l'ensemble des commits de Sofiane FOUDIL.
Cordialement, la Team E, composé de Sofiane FOUDIL et Nikola MILOSEVIC.