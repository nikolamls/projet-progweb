const express = require('express')
const router = express.Router()
const TypeCuisine = require('../models/typecuisine')
const Recette = require('../models/recette')

//route de tous les types de cuisine
router.get('/', async (req, res) => {
    let searchOptions = {}
    if (req.query.name != null  && req.query.name !== '') {
        searchOptions.name = new RegExp(req.query.name, 'i') //i = non sensible à la casse
    }
    //res.send('Bonjour le monde')
    try {
        const typescuisine = await TypeCuisine.find(searchOptions)
        res.render('typecuisine/index', {
            typescuisine: typescuisine,
            searchOptions: req.query
    }) // fait appel à la vue dans le dossier typecuisine
    } catch {
        res.redirect('/')
    }
}) //get a root

// nouveau type de cuisine route
router.get('/new', (req, res) => {
    res.render('typecuisine/new', {typeCuisine: new TypeCuisine()})// variables vont être envoyées vers le fichier ejs afin de créer un objet "type cuisine"
})

// création d'un nouveau type de cuisine
router.post('/', async (req, res) => {
    const typeCuisine = new TypeCuisine({
        name: req.body.name
    })

    try {
        const newTypeCuisine = await typeCuisine.save()
        res.redirect(`typecuisine/${newTypeCuisine.id}`)
        //res.redirect('typecuisine')
    } catch {
        res.render('typecuisine/new', {
            typeCuisine: typeCuisine,
            errorMessage: 'Erreur lors de la création du type de cuisine'
        })
    }

    
    //res.send(req.body.nom)
})


router.get('/:id', async (req, res) => {
    try {
        const typeCuisine = await TypeCuisine.findById(req.params.id)
        const recettes = await Recette.find({ typeCuisine: typeCuisine.id}).limit(5).exec()
        res.render('typecuisine/show', {
            typeCuisine: typeCuisine,
            recettesParCat: recettes
        })
    } catch {
        res.redirect('/')
    }
    //res.send('Montrer catégorie ' + req.params.id)
})

router.get('/:id/edit', async (req, res) => {
    try {
        const typeCuisine = await TypeCuisine.findById(req.params.id)
        res.render('typecuisine/edit', {typeCuisine: typeCuisine})
    } catch {
        res.redirect('/typecuisine')
    }
})

router.put('/:id', async (req, res) => {
    let typeCuisine
    try {
        typeCuisine = await TypeCuisine.findById(req.params.id)
        typeCuisine.name = req.body.name
        await typeCuisine.save()
        res.redirect(`/typecuisine/${typeCuisine.id}`)
        //res.redirect('typecuisine')
    } catch {
        if(typeCuisine == null) {
            res.redirect('/')
        } else {
            res.render('typecuisine/edit', {
                typeCuisine: typeCuisine,
                errorMessage: 'Erreur lors de la modification de la catégorie culinaire'
            })
        }
    }
    //res.send('Mettre à jour catégorie ' + req.params.id)
})

router.delete('/:id', async (req, res) => {
    let typeCuisine
    try {
        typeCuisine = await TypeCuisine.findById(req.params.id)
        await typeCuisine.remove()
        res.redirect('/typecuisine')
        //res.redirect('typecuisine')
    } catch {
        if(typeCuisine == null) {
            res.redirect('/')
        } else {
            res.redirect(`/typecuisine/${typeCuisine.id}`)
        }
    }
    //res.send('Supprimer catégorie ' + req.params.id)
})

module.exports = router