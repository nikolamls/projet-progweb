const express = require('express')
const router = express.Router()
const Recette = require('../models/recette')

router.get('/', async (req, res) => {
    //res.send('Bonjour le monde')
    let recettes = []
    try {
        recettes = await Recette.find().sort({dateCreation: 'desc'}).limit(10).exec()
    } catch {
        recettes = []
    }
    res.render('index', {recettes: recettes}) // fait appel à la vue dans le dossier index
}) //get a root

module.exports = router