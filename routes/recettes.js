const express = require('express')
const router = express.Router()
//const multer = require('multer') //librairie multer utilisé pour l'upload de fichier
//const path = require('path')
//const fs = require('fs')
const Recette = require('../models/recette')
const TypeCuisine = require('../models/typecuisine')
//const uploadPath = path.join('public', Recette.recetteImgChemin)
const imageTypes = ['image/jpeg', 'image/png']
/*const upload = multer({
    dest: uploadPath,
    fileFilter: (req, file, callback) => {
        callback(null, imageTypes.includes(file.mimetype))
    }
})
*/

//route de toutes les recettes
router.get('/', async (req, res) => {
    let query = Recette.find()
    if (req.query.nomRecette != null && req.query.nomRecette != '' ) {
        query = query.regex('nomRecette', new RegExp(req.query.nomRecette, 'i'))
    }
    //res.send('Toutes les recettes')
    try {
        const recettes = await query.exec()//Recette.find({})
        res.render('recettes/index', {
            recettes: recettes,
            searchOptions: req.query
        })
    } catch {
        res.redirect('/')
    }
    
}) 

// nouvelle recette route
router.get('/new', async (req, res) => {
    //res.send('Nouvelle recette')
    renderNewPage(res, new Recette())
})

// création d'une nouvelle recette
router.post('/', async (req, res) => {
    //const fileName = req.file != null ? req.file.filename : null
    const recette = new Recette({
        nomRecette: req.body.nomRecette,
        typeCuisine: req.body.typeCuisine,
        description: req.body.description,
        nbPersonne: req.body.nbPersonne,
        nbCalorie: req.body.nbCalorie,
        ingredients: req.body.ingredients,
        tpsPreparation: req.body.tpsPreparation,
        prix: req.body.prix,
        difficulte: req.body.difficulte,
        ustensiles: req.body.ustensiles
        //imageRecette: fileName
    })
    saveImageRecette(recette, req.body.imagePlat)
    //res.send('Création d\'une nouvelle recette')

    try {
        const newRecette = await recette.save()
        res.redirect(`recettes/${newRecette.id}`)
        //res.redirect('recettes')
    } catch {
        /*if(recette.imageRecette != null) {
            removeRecetteImg(recette.imageRecette)
        }*/
        renderNewPage(res, recette, true)
    }
})

/*function removeRecetteImg(fileName) {
    fs.unlink(path.join(uploadPath, fileName), err => {
        if (err) console.error(err)
    })
}*/

//Show recette route
router.get('/:id', async (req, res) => {
    try {
        const recette = await Recette.findById(req.params.id).populate('typeCuisine').exec()
        res.render('recettes/show', {recette: recette})
    } catch {
        res.redirect('/')
    }
})

//Edit recette route
router.get('/:id/edit', async (req, res) => {
    try {
        const recette = await Recette.findById(req.params.id)
        renderEditPage(res, recette)
    } catch {
        res.redirect('/')
    }
})


// Modification d'une nouvelle recette route
router.put('/:id', async (req, res) => {
    let recette

    try {
        recette = await Recette.findById(req.params.id)
        recette.nomRecette = req.body.nomRecette
        recette.typeCuisine = req.body.typeCuisine
        recette.description = req.body.description
        recette.nbPersonne = req.body.nbPersonne
        recette.nbCalorie = req.body.nbCalorie
        recette.ingredients = req.body.ingredients
        recette.tpsPreparation = req.body.tpsPreparation
        recette.prix = req.body.prix
        recette.difficulte = req.body.difficulte
        recette.ustensiles = req.body.ustensiles
        if (req.body.imagePlat != null && req.body.imagePlat !== '') {
            saveImageRecette(recette, req.body.imagePlat)
        }
        await recette.save()
        res.redirect(`/recettes/${recette.id}`)
    } catch {
        if (recette != null) {
            renderEditPage(res, recette, true)
        } else {
            redirect('/')
        }
    }
})

//Supprimer recette
router.delete('/:id', async(req, res) => {
    let recette
    try {
        recette = await Recette.findById(req.params.id)
        await recette.remove()
        res.redirect('/recettes')
    } catch {
        if (recette != null) {
            res.render('recettes/show', {
                recette: recette,
                errorMessage: 'Impossible de supprimer la recette'
            })
         } else {
                res.redirect('/')
            }
        }
})


async function renderNewPage(res, recette, hasError = false) {
    renderFormPage(res, recette, 'new', hasError)
}

async function renderEditPage(res, recette, hasError = false) {
    renderFormPage(res, recette, 'edit', hasError)
}

async function renderFormPage(res, recette, form, hasError = false) {
    try {
        const typescuisine = await TypeCuisine.find({}) //attendre que toutes les catégories culinaires soient chargées
        const params = {
            typescuisine: typescuisine,
            recette: recette
        }
        if (hasError) {
            if (form === 'edit') {
                params.errorMessage = 'Erreur lors de la modification de la recette'
            } else {
                params.errorMessage = 'Erreur lors de la création de la recette'
            }
        }
        res.render(`recettes/${form}`, params)
    } catch  {
        res.redirect('/recettes')
    }
}

function saveImageRecette(recette, imgRecetteEncoded) {
    if (imgRecetteEncoded == null) return
    const imagePlat = JSON.parse(imgRecetteEncoded)
    if (imagePlat != null && imageTypes.includes(imagePlat.type)) {
        recette.imageRecette = new Buffer.from(imagePlat.data, 'base64')
        recette.imageRecetteType = imagePlat.type
    }
}

module.exports = router